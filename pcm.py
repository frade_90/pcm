# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 09:17:22 2021

@author: frade
"""

import numpy as np
import pandas as pd
from pyomo.environ import *
import scipy.io as sio
import matplotlib.pyplot as plt
import CoolProp.CoolProp as CP
from functions import *

class pipe:
    def __init__(self,Ri,Re,L,material):
        self.Ri = Ri
        self.Re = Re
        self.L = L
        self.M = material
        
    @property
    def vol(self):
        return np.pi*(self.Re**2-self.Ri**2)*self.L
    
    @property
    def A_int(self):
        return 2*np.pi*self.Ri*self.L
        
    @property
    def A_ext(self):
        return 2*np.pi*self.Re*self.L
             
    @property
    def A_top(self):
        return np.pi*(self.Re**2-self.Ri**2)
    
    def mass(self):
        return self.M['density']*self.vol
    
    def Rc(self):
        return np.log(self.Re/self.Ri)/(2*np.pi*self.M['conductivity']*self.L)


class HTF:
    def __init__(self, Fluid):
        self.F = Fluid
        
    def conductivity(self,T):
        return CP.PropsSI('L', 'T', T + 273.15, 'Q', 0, self.F)       # thermal conductivity [W/mK]
         
    def density(self,T):
        return CP.PropsSI('D', 'T', T + 273.15,'Q', 0, self.F)      # density [kg/m3]
        
    def specific_heat(self,T):
        return CP.PropsSI('C', 'T', T + 273.15,'Q', 0, self.F)       # specific heat [J/kgK]
                
    def dinamic_viscosity(self,T):
        return CP.PropsSI('V', 'T', T + 273.15,'Q', 0, self.F)       # dynamic viscosity [Pa*s]
    
    def Reynolds(self, Dh, mw, T):
        return 4*mw/(self.dinamic_viscosity(T)*np.pi*Dh)
    
    def Prandtl(self,T):
        return self.specific_heat(T)*self.dinamic_viscosity(T)/self.conductivity(T)
        
class pcm:
    def __init__(self):
        self.density_s = 789                 # density [kg/m3]
        self.density_l = 750                 # density [kg/m3]
        self.specificic_heat_s = 1800        # scpecific heat of solid [J/kgK]
        self.specificic_heat_l = 2400        # scpecific heat of liquid [J/kgK]
        self.conductivity_s = 0.18           # thermal conductivity of solid [W/mK]
        self.conductivity_l = 0.19           # thermal conductivity of liquid [W/mK]
        self.Tm1 = 26.5                        # melting temperature [C]
        self.Tm2 = 27.5                        # melting temperature [C]
        self.hl = 206e3                      # latent heat of fusion [J/kg]
     
    def radial_mesh(self, N, Ri, Re):
        Delta_r = (Re - Ri)/N
        Rad_position = np.zeros(N)
        for i in range(N):
            Rad_position[i] = Ri + i*Delta_r 
        Rad_position = np.append(Rad_position, Re)
        return Rad_position

def thermal_resistances(pcm, htf, geometry1, geometry2, N):
    RCond = np.zeros(N)
    r = pcm.radial_mesh(N, geometry1.Re, geometry2.Ri) 
    for i in range(N):
        RCond[i] = np.log(r[i+1]/r[i])/(2*np.pi*pcm.conductivity_s*geometry1.L)
    RCond = np.append(geometry1.Rc(), RCond)
    RCond = np.append(RCond, geometry2.Rc())
    return RCond

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
hours = 24
TES_nodes = np.arange(6)
time = np.arange(1, hours*4+1, 1)
#time = np.arange(1, 3, 1)
#Text = 10 + 5*np.sin(2*np.pi/24*np.arange(1,24*4+1,1)/4-3)
Text = 18*np.ones(hours*4)
Tset = 50*np.ones(hours*4+1)
Dt = 900

Fluid = 'Water'    # Heat transfer fluid
Copper = {'density': 8.954e3, 'specific_heat': 380, 'conductivity': 386}    #[kg/m3, J/kgK, W/mK]
Brass = {'density': 8.8e3, 'specific_heat': 380, 'conductivity': 151}       #[kg/m3, J/kgK, W/mK]

F = HTF(Fluid)
int_pipe = pipe(0.0165, 0.0175, 1, Copper)
ext_jacket = pipe(0.064, 0.0665, 1, Brass)
tes = pcm()

Rc = thermal_resistances(tes, F, int_pipe, ext_jacket, np.size(TES_nodes)-2)
#Rc = [2.42610270e-01, 2.42610270e-01, 2.42610270e-01, 2.42610270e-01, 2.42610270e-01, 2.42610270e-01]
mw = 0.018
Tin = 45
Dh = 2*int_pipe.Ri

Reyn = F.Reynolds(Dh, mw, Tin)

if Reyn >= 2200:
    Nu = 3.66
else:
    Nu = 0.023*Reyn**0.8*F.Prandtl(Tin)**0.33

h_int = (Nu*F.conductivity(Tin))/Dh
h_ext = 0.005

R_conv_int = 1/(int_pipe.A_int*h_int)
R_conv_ext = 1/(ext_jacket.A_ext*h_ext)

DR = (ext_jacket.Re - int_pipe.Re)/(np.size(TES_nodes)-2)

Tmin = 20
Tmax = 50
H = [tes.specificic_heat_s*Tmin, tes.specificic_heat_s*tes.Tm1, tes.specificic_heat_l*tes.Tm2, tes.specificic_heat_l*Tmax]
T = [Tmin, tes.Tm1, tes.Tm2, Tmax]

r_pos = tes.radial_mesh(np.size(TES_nodes)-2, int_pipe.Re, ext_jacket.Re)

#PYOMO MODEL
model = ConcreteModel()

model.x_t = Var(TES_nodes, time, within = NonNegativeReals, bounds=(20,100), initialize=26)
model.x_h = Var(TES_nodes, time, within = NonNegativeReals)
model.gamma1 = Var(TES_nodes, time, within = Binary, bounds=(0,1), initialize=1)
model.gamma2 = Var(TES_nodes, time, within = Binary, bounds=(0,1), initialize=0)
model.gamma3 = Var(TES_nodes, time, within = Binary, bounds=(0,1), initialize=0)
model.eps1 = Var(TES_nodes, time, within = NonNegativeReals, bounds=(0,10), initialize=1)
model.eps2 = Var(TES_nodes, time, within = NonNegativeReals, bounds=(0,10), initialize=0)
model.eps3 = Var(TES_nodes, time, within = NonNegativeReals, bounds=(0,10), initialize=0)
model.eps4 = Var(TES_nodes, time, within = NonNegativeReals, bounds=(0,10), initialize=0)

model.u = Var(time, within = NonNegativeReals, bounds=(0,10e5))
model.x_w = Var(time, within = NonNegativeReals, bounds=(0,70), initialize=70)

" Objective function "
def Obj_function(model):
    """ Evaluate the operative cost 
        over the given time horizon """
    J = sum(-model.u[i] for i in time) 
    return J
model.obj = Objective(rule = Obj_function)

def initial_value_t(model,j):
    if j > TES_nodes[0] and j < TES_nodes[-1]:
        return Constraint.Skip
    else:
        #return model.x_t[j,1] == 26.5
        return model.x_t[j,1] == 20
model.cons_in_t = Constraint(TES_nodes, rule = initial_value_t)    

def initial_value_h(model,j):
    if j == TES_nodes[0] or j == TES_nodes[-1]:
        return Constraint.Skip
    else:
        #return model.x_h[j,1] == 47700*(tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L)
        return model.x_h[j,1] == 36e3*(tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L)
model.cons_in_h = Constraint(TES_nodes, rule = initial_value_h)


def dynamic(model,j,i):
    if i == 1:
        return Constraint.Skip  
    else:
        if j == TES_nodes[0]:
            return model.x_t[j,i] == (Dt/(int_pipe.mass()*int_pipe.M['specific_heat']))*((model.x_w[i]-model.x_t[j,i])/(R_conv_int + Rc[j]/2) - (model.x_t[j,i]-model.x_t[j+1,i])/(Rc[j]/2 + Rc[j+1]/2)) + model.x_t[j,i-1]
        
        elif j == TES_nodes[-1]:
            return model.x_t[j,i] == (Dt/(ext_jacket.mass()*ext_jacket.M['specific_heat']))*((model.x_t[j-1,i]-model.x_t[j,i])/(Rc[j]/2 + R_conv_ext/2)) + model.x_t[j,i-1]
        else:
            return model.x_h[j,i] == (Dt/(tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L))*((model.x_t[j+1,i] - model.x_t[j,i])/(Rc[j+1]/2 + Rc[j]/2) - (model.x_t[j,i] - model.x_t[j-1,i])/(Rc[j]/2 + Rc[j-1]/2)) + model.x_h[j,i-1]
model.cons_dyn = Constraint(TES_nodes, time, rule = dynamic)


def cons_pcm_h(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return H[0]*model.eps1[j,i] + H[1]*model.eps2[j,i] + H[2]*model.eps3[j,i] + H[3]*model.eps4[j,i] == model.x_h[j,i]/(tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L)
model.cons_pcm_h = Constraint(TES_nodes, time, rule = cons_pcm_h)


def cons_pcm_t(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return T[0]*model.eps1[j,i] + T[1]*model.eps2[j,i] + T[2]*model.eps3[j,i] + T[3]*model.eps4[j,i] == model.x_t[j,i] 
model.cons_pcm_t = Constraint(TES_nodes, time, rule = cons_pcm_t)


def cons_eps(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return model.eps1[j,i] + model.eps2[j,i] + model.eps3[j,i] + model.eps4[j,i] == 1
model.cons_eps = Constraint(TES_nodes, time, rule = cons_eps)


def cons_eps1(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return model.eps1[j,i] <= model.gamma1[j,i]        
model.cons_eps1 = Constraint(TES_nodes, time, rule = cons_eps1)


def cons_eps2(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return model.eps2[j,i] <= model.gamma1[j,i] + model.gamma2[j,i]       
model.cons_eps2 = Constraint(TES_nodes, time, rule = cons_eps2)


def cons_eps3(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
       return Constraint.Skip
    else:
       return model.eps3[j,i] <= model.gamma2[j,i] + model.gamma3[j,i]        
model.cons_eps3 = Constraint(TES_nodes, time, rule = cons_eps3)


def cons_eps4(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
        return Constraint.Skip
    else:
        return model.eps4[j,i] <= model.gamma3[j,i]        
model.cons_eps4 = Constraint(TES_nodes, time, rule = cons_eps4)


def cons_gamma(model,j,i):
    if j == 0 or j == TES_nodes[-1]:
        return Constraint.Skip
    else:
        return model.gamma1[j,i] + model.gamma2[j,i] + model.gamma3[j,i] == 1       
model.cons_gamma = Constraint(TES_nodes, time, rule = cons_gamma)

def cons_power(model,i):
    return model.u[i] == (model.x_w[i] - model.x_t[0,i])/(R_conv_int + Rc[0]/2)
model.cons_u = Constraint(time, rule = cons_power)

def cons_power_in(model):
    return model.u[1] == 0
model.cons_u_in = Constraint(rule = cons_power_in)

""" Choose the proper solver """
#solver = SolverFactory('glpk') 
#solver = SolverFactory('ipopt')
solver = SolverFactory('gurobi') 
#solver = SolverFactory('cplex',executable="/Applications/CPLEX_Studio_128/cplex/bin/x86-64_osx/cplexamp",solver_io="python")
    
solver.solve(model,tee=True)

Tw = [value(model.x_w[i]) for i in time]    
t0 = [value(model.x_t[0,i]) for i in time]
t1 = [value(model.x_t[1,i]) for i in time]
t2 = [value(model.x_t[2,i]) for i in time]
t3 = [value(model.x_t[3,i]) for i in time]
t4 = [value(model.x_t[4,i]) for i in time]
t5 = [value(model.x_t[5,i]) for i in time]
e1 = [value(model.eps1[1,i]) for i in time]
e2 = [value(model.eps2[1,i]) for i in time]
e3 = [value(model.eps3[1,i]) for i in time]
e4 = [value(model.eps4[1,i]) for i in time]
h1 = [value(model.x_h[1,i]) for i in time]    
h2 = [value(model.x_h[2,i]) for i in time]
h3 = [value(model.x_h[3,i]) for i in time]
h4 = [value(model.x_h[4,i]) for i in time]
g1 = [value(model.gamma1[1,i]) for i in time]
g2 = [value(model.gamma2[1,i]) for i in time]
g3 = [value(model.gamma3[1,i]) for i in time]

# DEBUG
def debug_dyn(model,j,i):
    if j == TES_nodes[0]:
       c1 = (Dt/(int_pipe.mass()*int_pipe.M['specific_heat']))
       R_l = (R_conv_int + Rc[j]/2)
       R_r = (Rc[j]/2 + Rc[j+1]/2)
       temp_o = model.x_t[j,i-1].value
       DT_l = model.x_w[i].value - model.x_t[j,i].value
       DT_r = model.x_t[j,i].value - model.x_t[j+1,i].value
       temp = (Dt/(int_pipe.mass()*int_pipe.M['specific_heat']))*((model.x_w[i].value-model.x_t[j,i].value)/(R_conv_int + Rc[j]/2) - (model.x_t[j,i].value - model.x_t[j+1,i].value)/(Rc[j]/2 + Rc[j+1]/2)) + model.x_t[j,i-1].value
       print('c1: ', c1)
       print('R_: ', R_l)
       print('R+:', R_r)
       print('DT_l:', DT_l)
       print('DT_r:', DT_r)
       print('Temp_in:', temp_o)
       print('Temp:', temp)
       
       return c1, R_l, R_r, temp_o, temp 
    elif j == TES_nodes[-1]:
       return (Dt/ext_jacket.mass()*ext_jacket.M['specific_heat'])*((Text[i-1]-model.x_t[j,i].value)/(R_conv_ext + Rc[j]/2) + (model.x_t[j-1,i].value-model.x_t[j,i].value)/(Rc[j]/2 + R_conv_ext/2)) + model.x_t[j,i-1].value
    else:
        mass = tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L
        c1 = (2*Dt)/(mass*(Rc[j+1]/2 + Rc[j]/2))
        c2 = (2*Dt)/(mass*(Rc[j]/2 + Rc[j-1]/2))
        DT1 = model.x_t[j+1,i].value - model.x_t[j,i].value
        DT2 = model.x_t[j,i].value - model.x_t[j-1,i].value
        H0 =  model.x_h[j,i-1].value
        Hnew = (Dt/(tes.density_s*2*np.pi*(r_pos[j]**2-r_pos[j-1]**2)*int_pipe.L))*((model.x_t[j+1,i].value - model.x_t[j,i].value)/(Rc[j+1]/2 + Rc[j]/2) - (model.x_t[j,i].value - model.x_t[j-1,i].value)/(Rc[j]/2 + Rc[j-1]/2)) + model.x_h[j,i-1].value
        print('c1: ', c1)
        print('c2: ', c2)
        print('DT1:', DT1)
        print('DT2:', DT2)
        print('H_in:', H0)
        print('H:', Hnew)

# def interpolation(H,T,Tx,M):
#      if Tx <= T[1]:
#          h = (tes.specificic_heat_s*Tx)
#      elif Tx > T[1] and Tx <= T[2]:
#          h = (H[1] + (H[2]-H[1])/(T[2]-T[1])*(Tx-T[1]))
#      else:
#          h = (tes.specificic_heat_l*Tx)
#      return h

# h = np.zeros(31)
# for i in np.arange(20,51,1):
#         h[i-20] = interpolation(H,T,i,mass)
        
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('Time (h)')
ax1.set_ylabel('T [C]', color=color)

t = time
ax1.plot(t*0.25, Tw,'r')
ax1.plot(t*0.25, t0,'b')
ax1.plot(t*0.25, t1,'--r')
ax1.plot(t*0.25, t2,'--g')
ax1.plot(t*0.25, t3,'--k')
ax1.plot(t*0.25, t4,'--b')
ax1.plot(t*0.25, t5,'k')
ax1.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()